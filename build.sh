#!/bin/bash
set -e

echo "libini Build System"
echo ""

APPLICATION="libprompt"

SOURCE="./src"
BINARY="./bin"
DIRSTRIBUTION="./dist"
LIBRARY="./lib"
RESOURCE="./res"
TEST="./test"

# Show command help and options
if [ "$1" = "help" ]; then
	echo " clean                  -  Delete all build artifacts"
  echo " resources              -  Build resource bundle"
  echo " compile                -  Compiles the source code"
  echo " link                   -  Links the object code with the libraries"
  echo " publish                -  Packages the executable in an archive"
	echo
	exit
fi

if [ "$1" = "clean" ] || [ -z $1 ]; then
  echo "Cleaning old build artifacts..."
  rm -fr $BINARY
  rm -fr $DIRSTRIBUTION
  rm -f $APPLICATION.a
fi

if [ "$1" = "resources" ] || [ -z $1 ]; then
  echo "Bundling resources..."
fi

if [ "$1" = "compile" ] || [ -z "$1" ]; then
  echo "Compiling..."
	mkdir bin
  cd $SOURCE
  gcc -c *.c -I.$LIBRARY
  cd ..
  mv $SOURCE/*.o $BINARY/
fi

if [ "$1" = "link" ] || [ -z "$1" ]; then
  echo "Linking..."
  cp $LIBRARY/**/*.a $BINARY/
	cd $BINARY
	for archive in *.a; do
		ar -x $archive
		rm $archive
	done
	cd ..
fi

if [ "$1" = "publish" ] || [ -z "$1" ]; then
  echo "Packaging..."
	mkdir $DIRSTRIBUTION
	ar rcs $BINARY/$APPLICATION.a $BINARY/*.o
	cp $BINARY/$APPLICATION.a $DIRSTRIBUTION/
	cp $SOURCE/*.h $DIRSTRIBUTION/
	cd $DIRSTRIBUTION
	tar -cf $APPLICATION.tar $APPLICATION.a *.h
	gzip $APPLICATION.tar
	rm *.a *.h
fi
