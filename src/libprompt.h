#ifndef __libprompt__
#define __libprompt__

/* Includes *******************************************************************/
#include <libcollection/list.h>

/* Types **********************************************************************/

/* Macros *********************************************************************/
#define ANSI_BLACK 30
#define ANSI_RED 31
#define ANSI_GREEN 32
#define ANSI_YELLOW 33
#define ANSI_BLUE 34
#define ANSI_MAGENTA 35
#define ANSI_CYAN 36
#define ANSI_WHITE 37

/* Global Functions ***********************************************************/
char *prompt(const char *text, int length);
int confirm(char *label, int defaultValue);
const char *choice(const char *label, List *choices, const char *defaultValue);
void error(const char *text);
void warning(const char *text);
void rainbow(const char *text);
char* ansi_color(int col, const char *message);
char* ansi_colorc(int col, char character);


#endif
