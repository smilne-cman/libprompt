#include <stdio.h>
#include <stdlib.h>
#include <libstring/libstring.h>
#include <libcollection/list.h>
#include <libcollection/compare.h>
#include "libprompt.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
char *prompt(const char *text, int length) {
  printf("%s ", text);
  char *value = (char *)malloc(sizeof(char) * length);

  fgets(value, length, stdin);
  trim(value);

  return value;
}

int confirm(char *label, int defaultValue) {
  char *message = (char *)malloc(sizeof(char) * 255);
  if(defaultValue) {
    sprintf(message, "%s [y/n](y):", label);
  } else {
    sprintf(message, "%s [y/n](n):", label);
  }

  char *reply = prompt(message, 255);
  free(message);

  if(strlen(trim(reply)) <= 0) return defaultValue;

  int result = (reply[0] == 'y' || reply[0] == 'Y');

  free(reply);
  return result;
}

const char *choice(const char *label, List *choices, const char *defaultValue) {
  char *message = (char *)malloc(sizeof(char) * 1024);

  if (defaultValue == NULL) {
    sprintf(message, "%s [%s]:", label, list_join(choices, ", ", 512));
  } else {
    sprintf(message, "%s [%s](%s):", label, list_join(choices, ", ", 512), defaultValue);
  }

  char *reply = prompt(message, 1024);

  if(strlen(trim(reply)) <= 0) return defaultValue;

  if(!list_contains(choices, reply, string_compare)) {
    sprintf(message, "%s is not a valid choice\n\n%s", reply, label);
    reply = choice(message, choices, defaultValue);
  }

  free(message);
  return reply;
}

void error(const char *text) {
	printf("%s\n", ansi_color(ANSI_RED, text));
}

void warning(const char *text) {
	printf("%s\n", ansi_color(ANSI_YELLOW, text));
}

char* ansi_color(int col, const char *message) {
	char *msg = (char*)malloc(1024 * sizeof(char));

	sprintf(msg, "\033[1;%im%s\033[0m", col, message);

	return msg;
}

char* ansi_colorc(int col, char character) {
	char *msg = (char*)malloc(1024 * sizeof(char));

	sprintf(msg, "\033[1;%im%c\033[0m", col, character);

	return msg;
}

void rainbow(const char *text) {
  int length = strlen(text);
  int colour = 30;
  int modifier = 1;

	for(int i = 0; i < length; i++) {
    printf("%s", ansi_colorc(colour += modifier, text[i]));

	  if(colour == 37) modifier = -1;
    if(colour == 30) modifier = 1;
  }

	 printf("\n");
}
